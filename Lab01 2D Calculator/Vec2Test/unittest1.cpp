#include "CppUnitTest.h"
#include "Vec2.h"

using namespace Engine;

namespace Microsoft {
	namespace VisualStudio {
		namespace CppUnitTestFramework {
			template<>
			std::wstring ToString<Vec2>( const Vec2& vec ) {
				return (L"( " + std::to_wstring( vec.x ) + L", " + std::to_wstring( vec.y) + L" )");
				}
			}
		}
	}

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Vec2Test
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD( Vec2Addition )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			Vec2 vec2 = Vec2( 3.0f, 2.0f );
			Vec2 vec3 = Vec2( 5.0f, 5.0f );
			Vec2 vec4 = Vec2( 10.0f, 10.0f );
			Vec2 vec5 = Vec2( 4.0f, 4.0f );
			Vec2 test = Vec2( 24.0f, 24.0f );
			Vec2 sum = vec1 + vec2 + vec3 + vec4 + vec5;
			Assert::AreEqual( sum, test );
			}

		TEST_METHOD( Vec2Subtraction )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			Vec2 vec2 = Vec2( 3.0f, 2.0f );
			Vec2 test = Vec2( -1.0f, 1.0f );
			Vec2 sum = vec1 - vec2;
			Assert::AreEqual( sum, test );
			}

		TEST_METHOD( Vec2FloatMultiply )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			float num = 3.0f;
			Vec2 test = Vec2( 6.0f, 9.0f );
			Vec2 product = vec1 * num;
			Assert::AreEqual( product, test );
			}

		TEST_METHOD( Vec2FloatMultiply2 )
			{
			//for the one outside of the class
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			float num = 3.0f;
			Vec2 test = Vec2( 6.0f, 9.0f );
			Vec2 product = num * vec1;
			Assert::AreEqual( product, test );
			}

		TEST_METHOD( Vec2Division )
			{
			Vec2 vec1 = Vec2( 6.0f, 6.0f );
			float num = 3.0f;
			Vec2 test = Vec2( 2.0f, 2.0f );
			Vec2 product = vec1 / num;
			Assert::AreEqual( product, test );
			}

		TEST_METHOD( Vec2Division2 )
			{
			//for the one outside of the class
			Vec2 vec1 = Vec2( 6.0f, 6.0f );
			float num = 3.0f;
			Vec2 test = Vec2( 2.0f, 2.0f );
			Vec2 product = num / vec1;
			Assert::AreEqual( product, test );
			}

		TEST_METHOD( Vec2DotProduct )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			Vec2 vec2 = Vec2( 3.0f, 2.0f );
			float test = 12.0f;
			Assert::AreEqual( test, vec1.Dot( vec2 ) );
			Vec2 perp1 = vec1.PerpCW();
			Assert::AreEqual( 0.0f, vec1.Dot( perp1 ) );
			Vec2 proj = ( ( vec1.Dot( vec2 ) ) / ( vec1.LengthSquared() ) ) * vec1;
			Vec2 testProj = ( ( test ) / ( vec1.LengthSquared() ) ) * vec1;
			Assert::AreEqual( proj, testProj );
			}

		TEST_METHOD( Vec2UnaryAdd )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			Assert::AreEqual( vec1, +vec1 );
			}

		TEST_METHOD( Vec2UnarySubtract )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			Vec2 vec2 = Vec2( -2.0f, -3.0f );
			Assert::AreEqual( -vec1, vec2 );
			}

		TEST_METHOD( Vec2Lerp )
			{
			Vec2 vec1 = Vec2( 2.0f, 3.0f );
			Vec2 vec2 = Vec2( 3.0f, 2.0f );
			Vec2 vec1Test = Vec2( 0.1f, 0.15f );
			Vec2 vec2Test = Vec2( 2.85f, 1.9f );
			float beta = 0.05f;
			Vec2 sum = Vec2( 2.95f, 2.05f );
			Assert::AreEqual( vec1Test, vec1.Lerp( Vec2(), (1-beta) ) );
			Assert::AreEqual( vec2Test, vec2.Lerp( Vec2(), beta ) );
			Assert::AreEqual( sum, vec2.Lerp( vec1, beta ) );
			}

		TEST_METHOD( Vec2Length )
			{
			Vec2 vec = Vec2( 2.0f, 3.0f );
			float testLength = 3.605551275463989f;
			Assert::IsTrue( vec.CompareDoubles( vec.Length(), testLength ) );
			}

		TEST_METHOD( Vec2LengthSquared )
			{
			Vec2 vec = Vec2( 2.0f, 3.0f );
			float testLength = 13.0f;
			Assert::IsTrue( vec.CompareDoubles( vec.LengthSquared(), testLength ) );
			}

		TEST_METHOD( Vec2Normalize )
			{
			Vec2 vec = Vec2(3.0f, 3.0f);
			Vec2 vecNorm = vec.Normalize();
			Vec2 test = vec / vec.Length();
			Assert::AreEqual( vecNorm, test );
			}

		TEST_METHOD( Vec2PerpCW )
			{
			Vec2 vec = Vec2(13.0f, 16.0f);
			Vec2 vecPerp = vec.PerpCW();
			Vec2 testPerp = Vec2( 16.0f, -13.0f );
			Assert::AreEqual( vecPerp, testPerp );
			}

		TEST_METHOD( Vec2PerpCCW )
			{
			Vec2 vec = Vec2(3.0f, 5.0f);
			Vec2 vecPerp = vec.PerpCCW();
			Vec2 testPerp = Vec2( -5.0f, 3.0f );
			Assert::AreEqual( vecPerp, testPerp );
			}

	};
}