#pragma once
#include "TabManager.h"

class DotProductTab
	{
		public:
		DotProductTab( RenderUI* pRenderUI );

		bool IsInitialized() const { return m_isInitialized; }

		~DotProductTab();

		private:
		bool m_isInitialized = false;
		bool Initialize( RenderUI* pRenderUI );
		bool ShutDown();
	};

