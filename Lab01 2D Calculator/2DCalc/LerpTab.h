#pragma once

class RenderUI;
class LerpTab
	{
		public:
		LerpTab(RenderUI* pRenderUI);
		~LerpTab();

		private:
		bool m_isInitialized = false;
		bool ShutDown();
		bool Initialize( RenderUI* pRenderUI );
	};

