#include "LerpTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;

namespace {
	Vec2 aVector;
	Vec2 bVector;
	Vec2 MinusVector;
	Vec2 aVectorLerpPortion;
	Vec2 bVectorLerpPortion;
	Vec2 lerpResultVector;

	void LerpTabCallBack( const LerpData& data ) {
		static int counter = 0;
		++counter;
		
		aVector = Vec2( data.a_i, data.a_j );
		bVector = Vec2( data.b_i, data.b_j );
		MinusVector = bVector - aVector;
		aVectorLerpPortion = aVector.Lerp(Vec2(), data.beta);
		bVectorLerpPortion = bVector.Lerp(Vec2(), 1- data.beta);
		lerpResultVector = aVector.Lerp( bVector, data.beta);
		}
	}

LerpTab::LerpTab( RenderUI* pRenderUI ) : m_isInitialized( false )
	{
	m_isInitialized = Initialize( pRenderUI );
	}

bool LerpTab::Initialize( RenderUI * pRenderUI )
	{
	pRenderUI->setLerpData( aVector.Pos(), bVector.Pos(), MinusVector.Pos(), aVectorLerpPortion.Pos(), bVectorLerpPortion.Pos(),
							lerpResultVector.Pos(), LerpTabCallBack);
	return false;
	}

bool LerpTab::ShutDown()
	{
	return false;
	}

LerpTab::~LerpTab()
	{
	ShutDown();
	}

