#include <iostream>
#include "AdditionTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;

namespace {
	Vec2 left;
	Vec2 right;
	Vec2 result;

	void AdditionTabCallBack( const BasicVectorEquationInfo& data ) {
		static int counter = 0;
		++counter;

		left = data.scalar1 * Vec2( data.x1, data.y1 );
		right = data.scalar2 * Vec2( data.x2, data.y2 );
		result = data.add ? ( left + right ) : ( left + -right );
		}
	}

AdditionTab::AdditionTab( RenderUI * pRenderUI ) : m_isInitialized( false )
	{
	m_isInitialized = Initialize( pRenderUI );
	}

bool AdditionTab::Initialize( RenderUI * pRenderUI )
	{
	pRenderUI->setBasicVectorEquationData( AdditionTabCallBack, left.Pos(), right.Pos(), result.Pos() );
	return true;
	}

bool AdditionTab::ShutDown()
	{
	return true;
	}

AdditionTab::~AdditionTab()
	{
	ShutDown();
	}
