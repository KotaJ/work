#include "DotProductTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;

namespace {
	Vec2 vector1;
	Vec2 vector2;
	Vec2 projectionVector;
	Vec2 rejectionVector;

	void DotProductDataCallBack( const DotProductData& data ) {
		static int counter = 0;
		++counter;

		vector1 = Vec2( data.v1i, data.v1j );
		vector2 = Vec2( data.v2i, data.v2j );
		projectionVector = ( ( vector1.Dot(vector2) ) / ( vector1.LengthSquared() ) ) * vector1;
		rejectionVector = vector2 - projectionVector;
		}
	}

DotProductTab::DotProductTab( RenderUI * pRenderUI ) : m_isInitialized( false )
	{
	m_isInitialized = Initialize( pRenderUI );
	}

bool DotProductTab::Initialize( RenderUI * pRenderUI )
	{
	pRenderUI->setDotProductData( vector1.Pos(), vector2.Pos(), projectionVector.Pos(), rejectionVector.Pos(), DotProductDataCallBack );
	return true;
	}

bool DotProductTab::ShutDown()
	{
	return true;
	}

DotProductTab::~DotProductTab()
	{
	ShutDown();
	}