#include "TabManager.h"
#include "RenderUI.h"
#include "AdditionTab.h"
#include "PerpendicularsTab.h"
#include "DotProductTab.h"
#include "LerpTab.h"


TabManager::TabManager( RenderUI * pRenderUI ) : m_isInitialized(false)
	{
	m_isInitialized = Initialize( pRenderUI );
	}

bool TabManager::Initialize( RenderUI * pRenderUI )
	{
	AdditionTab additionTab( pRenderUI );
	PerpendicularsTab perpendicularsTab( pRenderUI );
	DotProductTab dotProductTab( pRenderUI );
	LerpTab lerpTab( pRenderUI );
	return true;
	}

bool TabManager::ShutDown()
	{
	return true;
	}

TabManager::~TabManager()
	{
	ShutDown();
	}
