#include <iostream>
#include "RenderUI.h"
#include "TabManager.h"
#include "Engine.h"
#include "Vec2.h"

using namespace Engine;

int main(int argc, char** argv) {
	printf( "Hello, 2D Calculator!" );

	if( !Engine::Initialize() ) return -2;

	RenderUI renderUI;
	TabManager tabs( &renderUI );

	Vec2 v( 3.14f, 123.4f );
	std::cout << v << std::endl;

	if( !renderUI.initialize(argc, argv) ) return -1;
	renderUI.run();
	renderUI.shutdown();

	return 0;
	}