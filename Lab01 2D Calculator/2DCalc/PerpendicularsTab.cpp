#include "PerpendicularsTab.h"
#include "RenderUI.h"
#include "Vec2.h"

using namespace Engine;

namespace {
	Vec2 original;
	Vec2 normalized;
	Vec2 cwPerpendicularVector;
	Vec2 ccwPerpendicularVector;

	void PerpendicularsTabCallBack( const PerpendicularData& data ) {
		static int counter = 0;
		++counter;
		original = Vec2( data.x, data.y );
		normalized = original.Normalize();
		cwPerpendicularVector = original.PerpCW();
		ccwPerpendicularVector = original.PerpCCW();
		}
	}

PerpendicularsTab::PerpendicularsTab(RenderUI* pRenderUI) : m_isInitialized(false)
	{
	m_isInitialized = Initialize( pRenderUI );
	}

bool PerpendicularsTab::Initialize( RenderUI * pRenderUI )
	{
	pRenderUI->setPerpendicularData( original.Pos(), normalized.Pos(), cwPerpendicularVector.Pos(), ccwPerpendicularVector.Pos(), PerpendicularsTabCallBack );
	return false;
	}

bool PerpendicularsTab::ShutDown()
	{
	return false;
	}

PerpendicularsTab::~PerpendicularsTab()
	{
	ShutDown();
	}
