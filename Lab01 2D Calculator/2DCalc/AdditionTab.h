#pragma once
#include "TabManager.h"

class AdditionTab
	{
		public:
		AdditionTab( RenderUI* pRenderUI );

		bool IsInitialized() const { return m_isInitialized; }

		~AdditionTab();

		private:
		bool m_isInitialized = false;
		bool Initialize( RenderUI* pRenderUI );
		bool ShutDown();
	};

