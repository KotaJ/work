#pragma once
#include "TabManager.h"

class RenderUI;
class PerpendicularsTab
	{
		public:
		PerpendicularsTab( RenderUI* pRenderUI );

		bool IsInitialized() const { return m_isInitialized; }

		~PerpendicularsTab();

		private:
		bool m_isInitialized = false;
		bool Initialize( RenderUI* pRenderUI );
		bool ShutDown();
	};

