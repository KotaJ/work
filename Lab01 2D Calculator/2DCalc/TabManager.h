#pragma once

class RenderUI;
class TabManager
	{
		public:
		TabManager(RenderUI* pRenderUI);

		bool IsInitialized() const{ return m_isInitialized; }

		~TabManager();

		private:
		bool m_isInitialized = false;
		bool Initialize( RenderUI* pRenderUI );
		bool ShutDown();
	};

