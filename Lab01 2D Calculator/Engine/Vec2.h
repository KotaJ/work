#pragma once
#include <iostream>
#include "ExportHeader.h"
#include <math.h>
#include <assert.h>


namespace Engine {
	//Vec2 is an immutable vector class except for assignment
	class ENGINE_SHARED Vec2
		{
			public:
			Vec2() : Vec2( 0.0f, 0.0f ) {}
			Vec2( float xx, float yy ) : x(xx), y(yy) {}

			Vec2 operator+( const Vec2& right ) const;
			Vec2 operator-() const;
			Vec2 operator+() const;
			Vec2 operator*( float scale ) const { return Vec2( x*scale, y*scale ); }
			Vec2 operator-( const Vec2& right ) const { return ( *this + -right ); }
			Vec2 operator/( float scale ) const { return Vec2( x / scale, y / scale ); }
			bool operator==( const Vec2& right );

			bool CompareDoubles( float a, float b ) const;
			Vec2 Lerp( Vec2 v1, float t );
			float Length() const;
			float LengthSquared() const;
			Vec2 Normalize() const;
			Vec2 PerpCW() const;
			Vec2 PerpCCW() const;
			float Dot( Vec2 vec ) { return ( (x*vec.x) + (y*vec.y) ); }

			std::ostream& Output( std::ostream& os ) const;
			float* Pos() { return &x; }


		public:
			float x;
			float y;
		};

	inline std::ostream& operator<< ( std::ostream& os, const Vec2& v ) { return v.Output( os ); }
	inline Vec2 operator* ( float scale, const Vec2& v ) { return v * scale; }
	inline Vec2 operator/ ( float scale, const Vec2& v ) { return v / scale; }
	inline float Dot( Vec2 vec, Vec2 vec2 ) { return ( ( vec.x*vec2.y ) + ( vec.y*vec2.y ) ); }
	inline bool operator==( const Vec2 & left, const Vec2 & right )
		{
		bool equal = false;

		if( left.CompareDoubles( left.x, right.x ) && left.CompareDoubles( left.y, right.y ) ) equal = true;

		return equal;
		}


	}

