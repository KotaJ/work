#include "Vec2.h"

namespace Engine {
	Vec2 Engine::Vec2::operator+( const Vec2 & right ) const
		{
		return Vec2(x + right.x, y + right.y);
		}
	Vec2 Vec2::operator-() const
		{
		return Vec2(-x, -y);
		}
	Vec2 Vec2::operator+() const
		{
		return Vec2(x, y);
		}

	bool Vec2::operator==( const Vec2 & right )
		{
		bool equal = false;

		if( CompareDoubles( ( *this ).x, right.x ) && CompareDoubles( ( *this ).y, right.y ) ) equal = true;

		return equal;
		}

	bool Vec2::CompareDoubles( float a, float b ) const
		{
		float diff = a - b;
		float check = 0.000001f;
		return ((diff < check) && (-diff < check));
		}

	Vec2 Vec2::Lerp( Vec2 v1, float t )
		{
		assert( t >= 0),
			(t <= 1);
		return (( v1 * t ) + ( *this * (1-t) ));
		}

	float Vec2::Length() const
		{
		float length = LengthSquared();
		length = sqrt( length );

		return length;
		}

	float Vec2::LengthSquared() const
		{
		float length = 0.0f;

		length = ( x*x ) + ( y * y );

		return length;
		}

	Vec2 Vec2::Normalize() const
		{
		assert( LengthSquared() != 0 );
		Vec2 ret = (*this) / Length();
		return ret;
		}

	Vec2 Vec2::PerpCW() const
		{
		return Vec2(y, -x);
		}

	Vec2 Vec2::PerpCCW() const
		{
		return Vec2(-y, x);
		}

	std::ostream & Vec2::Output( std::ostream & os ) const
		{
		return os << "[" << x << ", " << y << "]";
		}
	}

