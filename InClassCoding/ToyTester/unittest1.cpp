#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\\InClassCoding\Toy.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ToyTester
{		
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestDoubler)
		{
			Toy toy;
			int i = 17;
			int j = toy.Doubler( i );
			Assert::AreEqual( j, i * 2 );
		}

		TEST_METHOD( TestTripler ) {
			Toy toy;
			int i = 17;
			int j = toy.Tripler( i );
			Assert::AreEqual( j, i * 3 );
			}

		TEST_METHOD( TestGetCurrentID ) {
			Toy toy;
			int actualId = toy.m_id;
			int currId = toy.GetCurrentID();
			Assert::AreEqual( actualId, currId );
			}

		TEST_METHOD( TestGetNewID ) {
			Toy toy;
			int actualID = ++toy.m_id;
			int currId = toy.GetNewID();
			Assert::AreEqual( actualID, currId );
			}

	};
}