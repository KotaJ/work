#pragma once
class Toy
	{
		public:
		int Doubler( int value ) const; //doubles the fun
		int Tripler( int value ) const;
		int GetNewID() { return ++m_id; }
		int GetCurrentID() const { return m_id; }

		public:
		int m_id = 0;
	};

